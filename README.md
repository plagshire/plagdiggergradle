# PlagDigger - An essentially different way to use Plag**

**PlagDigger** is a client application to use the [Plag** API](https://plague.io/api/)
from your desktop. It supports the default operations of the API and some additional *tools*.

## Features

The GUI supports different operations in association with the Plag**-API:
*   retrieve all cards from your personal archive
*   retrieve a set of subscribed cards
*   retrieve a set of cards which infected you
*   retrieve comments to a specific card
*   retrieve information about users
*   retrieve statistics to a specific card
*   upswipe/downswipe cards

There are some extra-functionionality like:
*   search for a card with keywords in a certain time interval
*   automatic upswipe/downswipe function under certain conditions
*    to be continued

## Dependencies
The current dependencies are:
*   Jackson [Core, Annotations, Databind]
*   Apache Log4j2 [Core, API]
*   JUnit

The dependencies are licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
(Jackson, Log4j2) and the [Eclipse Public License](http://www.eclipse.org/legal/epl-v10.html) (JUnit).
The build-management system of this project is Gradle, no need to load dependencies by your own.

## Installation
To install and run **PlagDigger**, simply use:
First make sure Gradle 2.4 and at least Java 8 is installed (don't forget to update the PATH-variable).
Then clone the project:
$ git clone git clone https://<your-username>@bitbucket.org/plagshire/plagdiggergradle.git
Change into the project directory and build the application:
$ gradle build

At least you can run the application with:
$ gradle run
or you use the default procedure:
$ java -jar /your/path/PlagDigger-[version].jar

## Issues/Bugs
If you have issues to run the application or find bugs please
report them to one of the contributors or use the [Issue-Tracker](https://bitbucket.org/plagshire/plagdiggergradle/issues)

## Misc
### License
Licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

### Distributors
+ Josy (JLotus) <josy[at]plagusers.tk>
+ Access Denied [base-functionality as Shell script, future distributor]

### Copyright
Copyright (c) 2015 by plagshire@BitBucket


